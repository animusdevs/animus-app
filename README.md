# README #

This README documents details about this repo.

### What is this repository for? ###

This is the repository made for Animus developers to work on the animus application.

### How do I get set up? ###

Download all the files and run the project file with visual studio.

### Contribution guidelines ###

Make sure that if something is removed, you detail what it was, and why it was removed. Also, when a change is made of any kind, alert Zeval in discord. Either via DM or in the #developers channel on Animus.

### Who do I talk to? ###

You should be able to talk to the developers about this repo. If they do not know what you would like to know about this repo. Go to Zeval and ask him directly.