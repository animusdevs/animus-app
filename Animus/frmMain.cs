﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Security.Principal;
using System.Net.Sockets;
using System.Threading;

namespace Animus
{
    public partial class frmMain : Form
    {

        public frmMain()
        {
            InitializeComponent();

            // Checks for Admin permissions.
            bool isadmin = IsAdministrator();
            if (isadmin != true)
            {
                MessageBox.Show("Please re-run this program as Administrator.");
                Environment.Exit(0);
            }
        }

        /// <summary>
        /// Removes borders from form.
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            if (m.Msg == WM_NCHITTEST)
                m.Result = (IntPtr)(HT_CAPTION);
        }

        private const int WM_NCHITTEST = 0x84;
        private const int HT_CLIENT = 0x1;
        private const int HT_CAPTION = 0x2;

        /// <summary>
        /// Initializes all visuals and creates a registry key to force newer IE version (browser).
        /// </summary>
        private void frmMain_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.None;
            this.BackgroundImage = Properties.Resources.animus;

            btnWebsite.Image = Properties.Resources.animus_button_website;
            btnWebsite.SizeMode = PictureBoxSizeMode.StretchImage;
            btnWebsite.Parent = this;
            btnWebsite.BackColor = Color.Transparent;
            btnWebsite.Location = new Point(12, 50);

            btnClose.Image = Properties.Resources.animus_button_exit;
            btnClose.SizeMode = PictureBoxSizeMode.StretchImage;
            btnClose.Parent = this;
            btnClose.BackColor = Color.Transparent;
            btnClose.Visible = true;
            btnClose.Location = new Point(935, 15);

            // Will create a registry key that will tell this app (Animus) to use the newer IE version.
            string installkey = @"SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION\";
             string entryLabel = AppDomain.CurrentDomain.FriendlyName;

             OperatingSystem osInfo = Environment.OSVersion;
            
             string version = osInfo.Version.Major.ToString() + '.' + osInfo.Version.Minor.ToString();
             uint editFlag = (uint)((version == "6.2") ? 0x2710 : 0x2328); // 6.2 = Windows 8 and therefore IE10

             RegistryKey existingSubKey = Registry.LocalMachine.OpenSubKey(installkey, false); // readonly key

             if (existingSubKey.GetValue(entryLabel) == null)
             {
                 existingSubKey = Registry.LocalMachine.OpenSubKey(installkey, true); // writable key
                 existingSubKey.SetValue(entryLabel, unchecked((int)editFlag), RegistryValueKind.DWord);
             }
        }

        /// <summary>
        /// Checks if the program was run as Admin.
        /// </summary>
        /// <returns>True or False</returns>
        public static bool IsAdministrator()
        {
            var identity = WindowsIdentity.GetCurrent();
            var principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        /// <summary>
        /// Loads the browser and removes extras.
        /// </summary>
        private void btnWebsite_Click(object sender, EventArgs e)
        {
            // Loads the website
            browser.Visible = true;
            browser.Navigate("http://animus.site88.net/");

            // Removes extras
            btnWebsite.Visible = false;

            // Resizes the app to be bigger (better viewing)
            this.Size = new Size(1200, 800);
            this.CenterToScreen();
            browser.Location = new Point(0, 0);
            browser.Size = new Size(this.Size.Width, this.Size.Height);

            // Creates new extras
            btnClose.Parent = browser;
            btnClose.Location = new Point(1145, 5);

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (browser.Visible == false)
                Application.Exit();
            else {
                browser.Visible = false;
                browser.Location = new Point(136, 46);
                this.Size = new Size(1000, 600);
                browser.Size = new Size(this.Size.Width, this.Size.Height);
                btnWebsite.Visible = true;
                btnClose.Parent = this;
                btnClose.Location = new Point(950, 15);
            }
            
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
